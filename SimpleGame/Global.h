#pragma once // 한번 밖에 안불린다는 뜻

#define MAX_OBJECTS 300		// 이 게임 안에서 맥시멈으로 만들어 낼 수 있는 OBJECTS는 300개다.
#define HERO_ID		0
#define MAP_ID		1

#define FIRE_NUM	10

#define WINDOW_SIZEX 1000
#define WINDOW_SIZEY 700

////////////////////////

#define GAME_STATE_TITLE	0
#define	GAME_STATE_INGAME	1
#define GAME_STATE_END		2


#define KIND_MAP		100
#define KIND_HERO		0
#define KIND_BULLET		1
#define KIND_BUILDING	2
#define KIND_ENEMY		3
#define KIND_BACKGROUND_LR	4
#define KIND_BACKGROUND_UD	5
#define KIND_DOOR_12		6
#define KIND_DOOR_21		7
#define KIND_DOOR_34		8
#define KIND_DOOR_43		9
#define KIND_DOOR_56		10
#define KIND_DOOR_65		11



#define KIND_FIRE_D		20
#define KIND_FIRE_U		21
#define KIND_FIRE_L		22
#define KIND_FIRE_R		23




// 스페셜 키
#define SHOOT_NONE -1
#define SHOOT_LEFT	0
#define SHOOT_RIGHT	1
#define SHOOT_UP	2
#define SHOOT_DOWN	3


// HP
#define HERO_HP		6
#define BULLET_HP	2
#define BUILDING_HP	20
#define BLOCK_HP	100
#define FIRE_HP		1

#define STATE_GROUND	0
#define STATE_AIR		1

#define GRAVITY		9.8

// ANIM STATE
#define ANIM_STATE_IDLE_UP		0
#define ANIM_STATE_IDLE_DOWN	1
#define ANIM_STATE_IDLE_LEFT	2
#define ANIM_STATE_IDLE_RIGHT	3

#define ANIM_STATE_WALK_UP		4
#define ANIM_STATE_WALK_DOWN	5
#define ANIM_STATE_WALK_LEFT	6
#define ANIM_STATE_WALK_RIGHT	7

#define ANIM_STATE_SHOT_UP		8
#define ANIM_STATE_SHOT_DOWN	9
#define ANIM_STATE_SHOT_LEFT	10
#define ANIM_STATE_SHOT_RIGHT	11

/*
#define ANIM_STATE_SHOT_IDLE_UP		12
#define ANIM_STATE_SHOT_IDLE_DOWN	13
#define ANIM_STATE_SHOT_IDLE_LEFT	14
#define ANIM_STATE_SHOT_IDLE_RIGHT	15*/


#define HERO_MAX_VELOCITY_X 3
#define HERO_MIN_VELOCITY_X -3
#define HERO_MAX_VELOCITY_Y 3
#define HERO_MIN_VELOCITY_Y -3
////////////////////////
