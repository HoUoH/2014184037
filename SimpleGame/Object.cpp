#include "stdafx.h"
#include "Object.h"
#include "Global.h"




Object::Object() {
	m_BulletCoolTime = 0.0f;
	m_BulletTime = 0.2f;
}

Object::~Object() {

}

void Object::InitBulletCooltime()
{
	m_BulletCoolTime = m_BulletTime;
}

bool Object::CanFireBullet()
{
	if (m_BulletCoolTime <= 0.f)
	{
		return true;
	}
	return false;
}

void Object::Update(float eTimeInSec) {

	// Process Cooltime
	m_BulletCoolTime -= eTimeInSec;

	if (m_BulletCoolTime <= 0.f)
	{
		m_BulletCoolTime = -1.f;
	}

	//Calc friction
	float gz = m_Mass * GRAVITY; // 수직항력
	float friction = m_FrictioncCoef * gz;

	float velMag = sqrtf(m_VelX * m_VelX + m_VelY * m_VelY + m_VelZ * m_VelZ);	// 벡터의 크기 구하기


	if (velMag < FLT_EPSILON)
	{
		m_VelX = 0.f;
		m_VelY = 0.f;
		m_VelZ = 0.f;

	}
	else
	{

		// 마찰력을 구하기 위해서는, 속도의 방향 벡터만 필요하다.
		// (크기가 1인 벡터 == 방향 벡터 == 정규화한 속도 벡터)
		float fx = -friction * m_VelX / velMag;		// 진행 방향 반대 방향 벡터(진행방향 벡터/진행 벡터 크기) * 마찰력(마찰계수 * 수직항력(질량 * 중력가속도))
		float fy = -friction * m_VelY / velMag;

		
		float accX = fx / m_Mass;
		float accY = fy / m_Mass;


		float newVelX = m_VelX + accX * eTimeInSec;	// 새 속도(마찰력이 더해진) = 원래 속도 + 가속도 * 순간 시간
		float newVelY = m_VelY + accY * eTimeInSec;	


		// 이거 때문에 아래 newVelX*m_VelX가 항상 양수
		/*m_VelX = newVelX;
		m_VelY = newVelY;*/

		if (newVelX * m_VelX < 0.f)
		{
			m_VelX = 0.f;
		}
		else
		{
			m_VelX = newVelX;
		}
		if (newVelY * m_VelY < 0.f)
		{
			m_VelY = 0.f;
		}
		else
		{
			m_VelY = newVelY;
		}
	
		// gravity calculation (z축에 따른 속도 갱신)
		m_VelZ = m_VelZ - GRAVITY * eTimeInSec;



	}


	//Calc velocity
	m_VelX = m_VelX + m_AccX * eTimeInSec;
	m_VelY = m_VelY + m_AccY * eTimeInSec;
	m_VelZ = m_VelZ + m_AccZ * eTimeInSec;

	



	//Calc location
	m_PosX = m_PosX + m_VelX * eTimeInSec;
	m_PosY = m_PosY + m_VelY * eTimeInSec;
	m_PosZ = m_PosZ + m_VelZ * eTimeInSec;

	if (m_PosZ > 0.f)
	{
		m_State = STATE_AIR;
	}
	else
	{
		m_State = STATE_GROUND;
		m_PosZ = 0.f;
		m_VelZ = 0.f;
	}
}

// 09.19 함수 선언에 elpasedTime이 추가됨.
void Object::ApplyForce(float x, float y, float z, float elapsedTime) 
{
	//float elapsedTime = 1.f;

	// calc acc
	m_AccX = x / m_Mass;
	m_AccY = y / m_Mass;
	m_AccZ = z / m_Mass;

	// calc velocity
	m_VelX = m_VelX + m_AccX * elapsedTime;
	m_VelY = m_VelY + m_AccY * elapsedTime;
	m_VelZ = m_VelZ + m_AccZ * elapsedTime;


	m_AccX = 0.f;
	m_AccY = 0.f;
	m_AccZ = 0.f;

}

void Object::GetPosition(float *x, float *y, float *z) {
	*x = m_PosX;
	*y = m_PosY;
	*z = m_PosZ;
}

void Object::GetSize(float *x, float *y, float *z) {
	*x = m_SizeX;
	*y = m_SizeY;
	*z = m_SizeZ;
}

void Object::GetVelocity(float *vX, float *vY, float *vZ) {
	*vX = m_VelX;
	*vY = m_VelY;
	*vZ = m_VelZ;
}

void Object::GetAcc(float *aX, float *aY, float *aZ) {
	*aX = m_AccX;
	*aY = m_AccY;
	*aZ = m_AccZ;
}


void Object::GetForce(float *fX, float *fY, float *fZ) {
	*fX = m_ForceX;
	*fY = m_ForceY;
	*fZ = m_ForceZ;
}

void Object::SetForce(float fX, float fY, float fZ) {
	fX = m_ForceX;
	fY = m_ForceY;
	fZ = m_ForceZ;
}

void Object::GetMass(float* mass)
{
	*mass = m_Mass;
}

void Object::SetMass(float mass)
{
	m_Mass = mass;
}


void Object::GetFrictionCoef(float *friction)
{
	*friction = m_FrictioncCoef;
}

void Object::SetFrictionCoef(float friction)
{
	m_FrictioncCoef = friction;
}

////////////////////////
void Object::GetKind(int *kind)
{
	*kind = m_Kind;
}

void Object::SetKind(int kind)
{
	m_Kind = kind;
}

void Object::GetColor(float *R, float *G, float *B, float *A)
{
	*R = m_R;
	*G = m_G;
	*B = m_B;
	*A = m_A;
}

void Object::SetColor(float R, float G, float B, float A)
{
	m_R = R;
	m_G = G;
	m_B = B;
	m_A = A;
}



void Object::GetMaxHP(int *MaxHP)
{
	*MaxHP = m_MaxHP;
}

void Object::SetMaxHP(int MaxHP)
{
	m_MaxHP = MaxHP;
}

void Object::GetCurHP(int *CurHP)
{
	*CurHP = m_CurHP;
}

void Object::SetCurHP(int CurHP)
{
	m_CurHP = CurHP;
}



void Object::GetState(int *state)
{
	*state = m_State;
}

void Object::SetState(int state)
{
	m_State = state;
}

void Object::GetAnimState(int *animState)
{
	*animState = m_AnimState;
}

void Object::SetAnimState(int animState)
{
	m_AnimState = animState;
}

////////////////////////

void Object::SetPosition(float x, float y, float z) {
	m_PosX = x;
	m_PosY = y;
	m_PosZ = z;
}

void Object::SetSize(float x, float y, float z) {
	m_SizeX = x;
	m_SizeY = y;
	m_SizeZ = z;
}

void Object::SetVelocity(float vX, float vY, float vZ) {
	m_VelX = vX;
	m_VelY = vY;
	m_VelZ = vZ;
}

void Object::SetAcc(float aX, float aY, float aZ) {
	m_AccX = aX;
	m_AccY = aY;
	m_AccZ = aZ;
}
