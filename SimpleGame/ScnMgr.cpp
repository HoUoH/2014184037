#include "stdafx.h"
#include "ScnMgr.h"

#include "Global.h"
int g_Link_UD_Walk_Seq = 0;
int g_Link_LR_Walk_Seq = 0;
int g_Link_Idle_Seq = 0;
int g_Link_Shot_Seq = 0;

int g_Fire_Seq = 0;
//int g_Link_Shot_Idle_Seq = 0;

float g_Seq_Time = 0;
float g_Seq_Time2 = 0;

float g_Fire_Time[10] = {0};

int sumOfObjectNum = 0;

int g_GameState = 0;

ScnMgr::ScnMgr(int width, int height) {

	m_Title = m_Renderer->CreatePngTexture("./Textures/TITLE.png");
	m_End = m_Renderer->CreatePngTexture("./Textures/End.png");
	// Create textures
	m_Map = m_Renderer->CreatePngTexture("./Textures/HyruleCastle1F.png");
	m_Texture = m_Renderer->CreatePngTexture("./Textures/IssacCry.png");
	m_SeqTexture = m_Renderer->CreatePngTexture("./Textures/sprites.png");
	m_BulletTexture = m_Renderer->CreatePngTexture("./Textures/bullet.png");
	m_WallTexture = m_Renderer->CreatePngTexture("./Textures/IssacWall.png");

	// Mario
	m_Mario_Run = m_Renderer->CreatePngTexture("./Textures/Mario/walk.png");

	// Link
	m_Link_Walk_D = m_Renderer->CreatePngTexture("./Textures/Link/walk_d.png");
	m_Link_Walk_U = m_Renderer->CreatePngTexture("./Textures/Link/walk_u.png");
	m_Link_Walk_L = m_Renderer->CreatePngTexture("./Textures/Link/walk_l.png");
	m_Link_Walk_R = m_Renderer->CreatePngTexture("./Textures/Link/walk_r.png");

	m_Link_Idle_D = m_Renderer->CreatePngTexture("./Textures/Link/idle_d.png");
	m_Link_Idle_U = m_Renderer->CreatePngTexture("./Textures/Link/idle_u.png");
	m_Link_Idle_L = m_Renderer->CreatePngTexture("./Textures/Link/idle_l.png");
	m_Link_Idle_R = m_Renderer->CreatePngTexture("./Textures/Link/idle_r.png");

	m_Link_Shot_D = m_Renderer->CreatePngTexture("./Textures/Link/shot_d.png");
	m_Link_Shot_U = m_Renderer->CreatePngTexture("./Textures/Link/shot_u.png");
	m_Link_Shot_L = m_Renderer->CreatePngTexture("./Textures/Link/shot_l.png");
	m_Link_Shot_R = m_Renderer->CreatePngTexture("./Textures/Link/shot_r.png");

	m_Heart_Full = m_Renderer->CreatePngTexture("./Textures/HP/full.png");
	m_Heart_Half = m_Renderer->CreatePngTexture("./Textures/HP/half.png");
	m_Heart_Empty = m_Renderer->CreatePngTexture("./Textures/HP/empty.png");

	// Wall
	m_block = m_Renderer->CreatePngTexture("./Textures/HyruleCastle1F/1_wall.png");
	m_block_UD = m_Renderer->CreatePngTexture("./Textures/HyruleCastle1F/1_wall.png");
	m_door = m_Renderer->CreatePngTexture("./Textures/HyruleCastle1F/1_wall.png");

	// Enemy
	m_Fire = m_Renderer->CreatePngTexture("./Textures/Enemy/fire.png");

	
	
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		m_Objects[i] = NULL;
	}

	m_Objects[HERO_ID] = new Object();

	// pZ가 0이면, STATE_GROUND 여야 한다.

	m_Objects[HERO_ID]->SetPosition(-14.5f, 3.f, 0.f);
	m_Objects[HERO_ID]->SetVelocity(0.f, 0.f, 0.f);
	m_Objects[HERO_ID]->SetAcc(0.f, 0.f, 0.f);
	m_Objects[HERO_ID]->SetSize(0.5f, 0.5f, 0.5f);				//1pixel 당 1cm로 기준을 잡을 것 ...
	m_Objects[HERO_ID]->SetForce(0.f, 0.f, 0.f);
	m_Objects[HERO_ID]->SetMass(0.01f);
	m_Objects[HERO_ID]->SetFrictionCoef(0.8f);
	m_Objects[HERO_ID]->SetKind(KIND_HERO);
	m_Objects[HERO_ID]->SetState(STATE_GROUND);
	m_Objects[HERO_ID]->SetAnimState(ANIM_STATE_IDLE_DOWN);
	m_Objects[HERO_ID]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_Objects[HERO_ID]->SetCurHP(HERO_HP);

	float fwidth = (float)m_ScreenWidth / 100.f;
	float fheight = (float)m_ScreenHeight / 100.f;

	float map_sX = 40.f, map_sY = 40.f, map_sZ = 50.f;


	//if (g_GameState == GAME_STATE_INGAME) {
		AddObject(
			0.0f, 0.f, 0.f,
			map_sX, map_sY, 1.f,
			0.0f, 0.f, 0.f,
			KIND_MAP,
			STATE_GROUND,
			ANIM_STATE_WALK_DOWN,
			0, 0
		);

		for (int i = 0; i <= 10; i++) {
			AddObject(
				-5.f + i, -3.5f, 0.0f,
				1.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BUILDING,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BUILDING_HP,
				BUILDING_HP);
		}

		/*for (int i = 0; i <= 10; i++) {
			AddObject(
				-5.f + i, 3.f, 0.0f,
				1.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BUILDING,
				STATE_GROUND,
				BUILDING_HP,
				BUILDING_HP);
		}*/

		// Add enemy
		AddObject(
			3.f, 0.f, 0.0f,
			1.f, 1.f, 1.f,
			0.f, 0.f, 0.f,
			KIND_ENEMY,
			STATE_GROUND,
			ANIM_STATE_WALK_DOWN,
			BUILDING_HP,
			BUILDING_HP);


		// 1st stage background
		{
			// 왼 긴벽
			AddObject(
				-16.8f, 10.f, 0.f,
				3.f, 20.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 오 긴벽
			AddObject(
				-11.6f, 5.8f, 0.f,
				3.f, 12.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 아래 막기
			AddObject(
				-14.2f, 1.8f, 0.f,
				2.5f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_UD,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 오 꺾이는 부분
			AddObject(
				-11.8f, 11.7f, 0.f,
				2.5f, 0.2f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_UD,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 윗부분
			AddObject(
				-13.8f, 14.3f, 0.f,
				3.f, 0.2f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_UD,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);





			// 오른쪽 문 윗 부분
			AddObject(
				-12.f, 12.2f, 0.f,
				0.5f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			//오른쪽 문 아랫부분
			AddObject(
				-12.f, 14.2f, 0.f,
				0.5f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// DOOR_12
			AddObject(
				-11.5f, 13.2f, 0.f,
				1.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_DOOR_12,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);
		}

		// Stage 2 
		{
			// DOOR_21
			AddObject(
				-8.8f, 13.2f, 0.f,
				1.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_DOOR_21,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 위 왼벽
			AddObject(
				-6.1f, 14.9f, 0.f,
				7.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_UD,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 위 오른벽
			AddObject(
				2.f, 14.9f, 0.f,
				7.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_UD,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 문 왼위벽
			AddObject(
				-8.6f, 14.0f, 0.f,
				1.f, .5f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 문 왼아래벽
			AddObject(
				-8.6f, 12.5f, 0.f,
				1.f, 0.5f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 아래 긴벽
			AddObject(
				-2.2f, 12.0f, 0.f,
				12.5f, 0.5f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_UD,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 문 오른위벽
			AddObject(
				4.2f, 14.0f, 0.f,
				1.f, .5f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// 문 오른아래벽
			AddObject(
				4.2f, 12.5f, 0.f,
				1.f, 0.5f, 1.f,
				0.f, 0.f, 0.f,
				KIND_BACKGROUND_LR,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);

			// DOOR_34
			AddObject(
				4.5f, 13.3f, 0.f,
				1.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_DOOR_34,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);
		}

		{
			// DOOR_43
			AddObject(
				7.f, 13.2f, 0.f,
				1.f, 1.f, 1.f,
				0.f, 0.f, 0.f,
				KIND_DOOR_21,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				BLOCK_HP,
				BLOCK_HP
			);
		}
	//}

	// Initialize Renderer
	m_Renderer = new Renderer(width, height);

	m_ScreenWidth = width;
	m_ScreenHeight = height;

	

	if (!m_Renderer->IsInitialized()) {
		std::cout << "Renderer could not be initialized.. \n";
	}

	m_Sound = new Sound();
	m_SoundBG = m_Sound->CreateSound("./Sounds/ZeldaMainTheme.ogg");
	m_SoundFire = m_Sound->CreateSound("./Sounds/FireSound.ogg");
	m_SoundExploision = m_Sound->CreateSound("./Sounds/BeatSound.ogg");

	m_Sound->PlaySound(m_SoundBG, true, 0.5f);
}

ScnMgr::~ScnMgr() {

}

void ScnMgr::RenderScene() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.f, 1.f, 1.f, 1.f);

	
	// UI
	
	if (g_GameState == GAME_STATE_TITLE) {

		m_Renderer->SetCameraCenterPos(-1500.f, 1000.f);

		m_Renderer->DrawTextureRectDepth
		(
			-1500.f, 1000.f, 0.f,
			m_ScreenWidth, m_ScreenHeight,
			1.f, 1.f, 1.f, 1.f,
			m_Title,
			0.0f
		);
	}
	

	
		float x, y, z;
		float sX, sY, sZ;
		float R, G, B, A;
		float newX, newY, newZ, newSX, newSY, newSZ;
		float newW, newH;
		float bulletSize = 30.f;
		int AnimState;
		int MaxHP;
		int CurHP;
		int kind;

		struct Link_UD_Walk {
			int TotalSeqX = 8, TotalSeqY = 1;
			int CurSeqX = 0, CurSeqY = 0;
		}Link_UD_Walk;

		struct Link_LR_Walk {
			int TotalSeqX = 6, TotalSeqY = 1;
			int CurSeqX = 0, CurSeqY = 0;
		}Link_LR_Walk;

		struct Link_Idle {
			int TotalSeqX = 1, TotalSeqY = 1;
			int CurSeqX = 0, CurSeqY = 0;
		}Link_Idle;

		struct Link_Shot {
			int TotalSeqX = 3, TotalSeqY = 1;
			int CurSeqX = 0, CurSeqY = 0;
		}Link_Shot;

		struct Fire {
			int TotalSeqX = 4, TotalSeqY = 1;
			int CurSeqX = 0, CurSeqY = 0;
		}Fire;

		/*struct Link_Shot_Idle {
			int TotalSeqX = 1, TotalSeqY = 1;
			int CurSeqX = 0, CurSeqY = 0;
		}Link_Shot_Idle;*/



		// Idle
		Link_Idle.CurSeqX = g_Link_Idle_Seq % Link_Idle.TotalSeqX;
		Link_Idle.CurSeqY = g_Link_Idle_Seq % Link_Idle.TotalSeqY;

		// UD_Walk
		Link_UD_Walk.CurSeqX = g_Link_UD_Walk_Seq % Link_UD_Walk.TotalSeqX;
		Link_UD_Walk.CurSeqY = g_Link_UD_Walk_Seq % Link_UD_Walk.TotalSeqY;

		// LR_Walk
		Link_LR_Walk.CurSeqX = g_Link_LR_Walk_Seq % Link_LR_Walk.TotalSeqX;
		Link_LR_Walk.CurSeqY = g_Link_LR_Walk_Seq % Link_LR_Walk.TotalSeqY;

		// Shot
		Link_Shot.CurSeqX = g_Link_Shot_Seq % Link_Shot.TotalSeqX;
		Link_Shot.CurSeqY = g_Link_Shot_Seq % Link_Shot.TotalSeqY;

		// Fire
		Fire.CurSeqX = g_Fire_Seq % Fire.TotalSeqX;
		Fire.CurSeqY = g_Fire_Seq % Fire.TotalSeqY;


		/*// Shot_Idle
		Link_Shot_Idle.CurSeqX = g_Link_Shot_Seq % Link_Shot_Idle.TotalSeqX;
		Link_Shot_Idle.CurSeqY = g_Link_Shot_Seq % Link_Shot_Idle.TotalSeqY;*/



		// 애니메이션 시퀀스 속도 조절
		if (g_Seq_Time > 0.1f) {
			g_Link_UD_Walk_Seq++;
			g_Link_LR_Walk_Seq++;

			// Link UD Walk
			if (g_Link_UD_Walk_Seq > 8)
				g_Link_UD_Walk_Seq = 0;

			// Link LR Walk
			if (g_Link_LR_Walk_Seq > 6)
				g_Link_LR_Walk_Seq = 0;

			g_Seq_Time = 0.f;
		}

		if (g_Seq_Time2 > 0.05f) {
			g_Link_Shot_Seq++;
			g_Fire_Seq++;

			// Link Shot
			if (g_Link_Shot_Seq > 3)
				g_Link_Shot_Seq = 0;

			// Fire
			if (g_Fire_Seq > 4)
				g_Fire_Seq = 0;

			g_Seq_Time2 = 0.f;

		}
		
		if (g_GameState == GAME_STATE_INGAME)
		{
			// 오브젝트들
			for (int i = 0; i < MAX_OBJECTS; i++) {
				if (m_Objects[i]) {
					m_Objects[i]->GetPosition(&x, &y, &z);
					m_Objects[i]->GetSize(&sX, &sY, &sZ);
					m_Objects[i]->GetColor(&R, &G, &B, &A);
					m_Objects[i]->GetKind(&kind);
					m_Objects[i]->GetAnimState(&AnimState);
					m_Objects[i]->GetMaxHP(&MaxHP);
					m_Objects[i]->GetCurHP(&CurHP);

					newX = x * 100.f; newY = y * 100.f; newZ = z * 100.f;
					newSX = sX * 100.f; newSY = sY * 100.f; newSZ = sZ * 100.f;
					newW = newSX * 1.f; newH = 50.f;



					if (i == HERO_ID)
					{
						float slot1_pX = newX, slot1_pY = newY, slot1_pZ = newZ;
						//float slot2_pX = 0.f, slot2_pY = 0.f, slot2_pZ = 0.f;
						//float slot3_pX = 0.f, slot3_pY = 0.f, slot3_pZ = 0.f;
						float HP_sX = 50.f, HP_sY = 50.f;

						m_Renderer->SetCameraCenterPos(newX, newY);

						// 하트 UI

						if (CurHP == 6) {
							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 400.f, slot1_pY + 280.f, 0,
								HP_sX, HP_sY,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 340.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 280.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);
						}
						if (CurHP == 5) {
							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 400.f, slot1_pY + 280.f, 0,
								HP_sX, HP_sY,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 340.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 280.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Half,
								0.4f
							);
						}
						if (CurHP == 4) {
							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 400.f, slot1_pY + 280.f, 0,
								HP_sX, HP_sY,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 340.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 280.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);
						}
						if (CurHP == 3) {
							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 400.f, slot1_pY + 280.f, 0,
								HP_sX, HP_sY,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 340.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Half,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 280.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);
						}
						if (CurHP == 2) {
							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 400.f, slot1_pY + 280.f, 0,
								HP_sX, HP_sY,
								1, 1, 1, 1,
								m_Heart_Full,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 340.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 280.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);
						}
						if (CurHP == 1) {
							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 400.f, slot1_pY + 280.f, 0,
								HP_sX, HP_sY,
								1, 1, 1, 1,
								m_Heart_Half,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 340.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 280.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);
						}
						if (CurHP == 0) {
							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 400.f, slot1_pY + 280.f, 0,
								HP_sX, HP_sY,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 340.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);

							m_Renderer->DrawTextureRectDepth(
								slot1_pX - 280.f, slot1_pY + 280.f, 0,
								50, 50,
								1, 1, 1, 1,
								m_Heart_Empty,
								0.4f
							);
						}
					}

					// 링크 애니메이션
					if (kind == KIND_HERO) {
						if (AnimState == ANIM_STATE_IDLE_UP) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Idle_U,
								Link_Idle.CurSeqX, Link_Idle.CurSeqY,
								Link_Idle.TotalSeqX, Link_Idle.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_IDLE_DOWN) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Idle_D,
								Link_Idle.CurSeqX, Link_Idle.CurSeqY,
								Link_Idle.TotalSeqX, Link_Idle.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_IDLE_LEFT) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Idle_L,
								Link_Idle.CurSeqX, Link_Idle.CurSeqY,
								Link_Idle.TotalSeqX, Link_Idle.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_IDLE_RIGHT) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Idle_R,
								Link_Idle.CurSeqX, Link_Idle.CurSeqY,
								Link_Idle.TotalSeqX, Link_Idle.TotalSeqY,
								0
							);
						}

						///////////////////////////////////////////////
						if (AnimState == ANIM_STATE_WALK_UP) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Walk_U,
								Link_UD_Walk.CurSeqX, Link_UD_Walk.CurSeqY,
								Link_UD_Walk.TotalSeqX, Link_UD_Walk.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_WALK_DOWN) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Walk_D,
								Link_UD_Walk.CurSeqX, Link_UD_Walk.CurSeqY,
								Link_UD_Walk.TotalSeqX, Link_UD_Walk.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_WALK_LEFT) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Walk_L,
								Link_LR_Walk.CurSeqX, Link_LR_Walk.CurSeqY,
								Link_LR_Walk.TotalSeqX, Link_LR_Walk.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_WALK_RIGHT) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Walk_R,
								Link_LR_Walk.CurSeqX, Link_LR_Walk.CurSeqY,
								Link_LR_Walk.TotalSeqX, Link_LR_Walk.TotalSeqY,
								0
							);
						}

						//////////////////////////////////////////////////////////
						if (AnimState == ANIM_STATE_SHOT_UP) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Shot_U,
								Link_Shot.CurSeqX, Link_Shot.CurSeqY,
								Link_Shot.TotalSeqX, Link_Shot.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_SHOT_DOWN) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Shot_D,
								Link_Shot.CurSeqX, Link_Shot.CurSeqY,
								Link_Shot.TotalSeqX, Link_Shot.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_SHOT_LEFT) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Shot_L,
								Link_Shot.CurSeqX, Link_Shot.CurSeqY,
								Link_Shot.TotalSeqX, Link_Shot.TotalSeqY,
								0
							);
						}
						if (AnimState == ANIM_STATE_SHOT_RIGHT) {
							m_Renderer->DrawTextureRectSeqXYDepth(
								newX, newY, 0,
								newSX, newSY,
								R, G, B, A,
								m_Link_Shot_R,
								Link_Shot.CurSeqX, Link_Shot.CurSeqY,
								Link_Shot.TotalSeqX, Link_Shot.TotalSeqY,
								0
							);
						}
					}

					// Fire Anim
					if (KIND_FIRE_D <= kind && kind <= KIND_FIRE_R) {
						m_Renderer->DrawTextureRectSeqXYDepth(
							newX, newY, 0,
							newSX, newSY,
							R, G, B, A,
							m_Fire,
							Fire.CurSeqX, Fire.CurSeqY,
							Fire.TotalSeqX, Fire.TotalSeqY,
							0
						);
					}


					if (kind == KIND_BULLET) {
						m_Renderer->DrawTextureRectHeight(
							newX, newY, 0,
							newSX, newSY,
							R, G, B, A,
							m_BulletTexture,
							newZ);
					}

					if (kind == KIND_BUILDING) {
						m_Renderer->DrawTextureRectHeight(
							newX, newY, 0,
							newSX, newSY,
							R, G, B, A,
							m_WallTexture,
							newZ);
					}

					if (kind == KIND_ENEMY) {
						m_Renderer->DrawTextureRectHeight(
							newX, newY, 0,
							newSX, newSY,
							R, G, B, A,
							m_Texture,
							newZ);
					}

					if (kind == KIND_MAP) {
						m_Renderer->DrawTextureRectSeqXYDepth(
							newX, newY, 0,
							newSX, newSY,
							R, G, B, A,
							m_Map,
							0, 0,
							1, 1,
							1.f
						);
					}

					if (kind == KIND_BACKGROUND_LR) {
						m_Renderer->DrawTextureRectDepth(
							newX, newY, 0,
							newSX, newSY,
							1.f, 1.f, 1.f, 1.f,
							m_block,
							0.1f
						);
					}

					if (kind == KIND_BACKGROUND_UD) {
						m_Renderer->DrawTextureRectDepth(
							newX, newY, 0,
							newSX, newSY,
							1.f, 1.f, 1.f, 1.f,
							m_block_UD,
							0.1f
						);
					}
					if (kind == KIND_DOOR_12 || kind == KIND_DOOR_21 || kind == KIND_DOOR_34 || kind == KIND_DOOR_43) {
						m_Renderer->DrawTextureRectDepth(
							newX, newY, 0,
							newSX, newSY,
							1.f, 1.f, 1.f, 1.f,
							m_door,
							0.1f
						);
					}

				}

			}
		}
	
		if (g_GameState == GAME_STATE_END)
		{
			m_Renderer->SetCameraCenterPos(-1500.f, 1000.f);

			m_Renderer->DrawTextureRectDepth
			(
				-1500.f, 1000.f, 0.f,
				m_ScreenWidth, m_ScreenHeight,
				1.f, 1.f, 1.f, 1.f,
				m_End,
				0.0f
			);
		}

	


	

}

void ScnMgr::Update(float elapsedTime)
{
	if (g_GameState == GAME_STATE_INGAME) {
		UpdateCollision();
		g_Seq_Time += elapsedTime;
		g_Seq_Time2 += elapsedTime;

		for (int i = 0; i < FIRE_NUM; ++i)
			g_Fire_Time[i] += elapsedTime;

		// Fire1
		if (g_Fire_Time[0] > 2.0f) {
			AddObject(
				-14.9f, 14.3f, 0.f,
				.5f, .4f, .5f,
				0.f, -2.f, 0.f,
				KIND_FIRE_D,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				FIRE_HP,
				FIRE_HP
			);
			g_Fire_Time[0] = 0.5f;
		}

		if (g_Fire_Time[1] > 2.5f) {
			// Fire2
			AddObject(
				-14.1f, 14.3f, 0.f,
				.5f, .4f, .5f,
				0.f, -2.f, 0.f,
				KIND_FIRE_D,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				FIRE_HP,
				FIRE_HP
			);
			g_Fire_Time[1] = 0.9f;
		}

		if (g_Fire_Time[2] > 3.0f) {
			// Fire3
			AddObject(
				-13.4f, 14.3f, 0.f,
				.5f, .4f, .5f,
				0.f, -2.f, 0.f,
				KIND_FIRE_D,
				STATE_GROUND,
				ANIM_STATE_WALK_DOWN,
				FIRE_HP,
				FIRE_HP
			);
			g_Fire_Time[2] = 1.5f;
		}

		//float velX, velY, velZ;
		//m_Objects[HERO_ID]->GetVelocity(&velX, &velY, &velZ);
		//std::cout << "velX, velY : " << velX << ", " << velY;

		int kind;
		float velX, velY, velZ;

		for (int i = 0; i < MAX_OBJECTS; i++)
		{
			if (m_Objects[i])
			{
				if (i == HERO_ID) {
					m_Objects[HERO_ID]->GetVelocity(&velX, &velY, &velZ);
					if (velX > HERO_MAX_VELOCITY_X)
						velX = HERO_MAX_VELOCITY_X;
					if (velX < HERO_MIN_VELOCITY_X)
						velX = HERO_MIN_VELOCITY_X;
					if (velY > HERO_MAX_VELOCITY_Y)
						velY = HERO_MAX_VELOCITY_Y;
					if (velY < HERO_MIN_VELOCITY_Y)
						velY = HERO_MIN_VELOCITY_Y;

					m_Objects[HERO_ID]->SetVelocity(velX, velY, velZ);
				}

				m_Objects[i]->GetKind(&kind);
				if (kind == KIND_FIRE_D) {
					m_Objects[i]->SetVelocity(0.f, -3.f, 0.f);
				}

				m_Objects[i]->Update(elapsedTime);

			}

			//delete(m_Objects[i]);

		}
		int curHP;
		m_Objects[HERO_ID]->GetCurHP(&curHP);

		if (curHP == 0)
			g_GameState = GAME_STATE_END;
	}
	//m_Objects[HERO_ID]->Update(elapsedTime);
}

void ScnMgr::ApplyForce(float x, float y, float z, float elapsedTime)
{
	int state;
	m_Objects[HERO_ID]->GetState(&state);

	if (state == STATE_AIR)
	{
		z = 0;
	}

	m_Objects[HERO_ID]->ApplyForce(x, y, z, elapsedTime);
}


void ScnMgr::AddObject(float pX, float pY, float pZ, float sX, float sY, float sZ, float vX, float vY, float vZ, int kind, int state, int animState, int maxHP, int hp)
{
	int index = FindEmptyObjectSlot();

	if (index < 0)
	{
		std::cout << "Can't create object with minus index. \n";
		return;
	}
	m_Objects[index] = new Object();
	m_Objects[index]->SetPosition(pX, pY, pZ);
	m_Objects[index]->SetVelocity(vX, vY, vZ);
	m_Objects[index]->SetAcc(0.f, 0.f, 0.f);
	m_Objects[index]->SetSize(sX, sY, sZ);				//1pixel 당 1cm로 기준을 잡을 것 ...

	m_Objects[index]->SetForce(1.f, 0.f, 0.f);
	m_Objects[index]->SetMass(0.01f);
	m_Objects[index]->SetFrictionCoef(0.2f);
	m_Objects[index]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_Objects[index]->SetKind(kind);
	m_Objects[index]->SetState(state);
	m_Objects[index]->SetAnimState(animState);
	m_Objects[index]->SetMaxHP(maxHP);
	m_Objects[index]->SetCurHP(hp);
	
	
}

void ScnMgr::LatestDirectionInput(int dir)
{

	if (m_Objects[HERO_ID] != NULL)
	{
		m_Objects[HERO_ID]->SetAnimState(dir);
	}

}

void ScnMgr::DeleteObject(unsigned int id)
{
	if (m_Objects[id])			// NULL 이 아니면 delete 시킨다
	{
		delete m_Objects[id];
		m_Objects[id] = NULL;	// ★ delete 한 후에 NULL로 초기화 해 줄 것 ★★★★★ 매우 중요 ★★★★★
	}
}




int ScnMgr::FindEmptyObjectSlot()
{
	for (int i = 1; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			return i;
	}
	std::cout << "Object list is full. \n";
	sumOfObjectNum++;
	return -1;
}
/////////////////////////

void ScnMgr::Shoot(int ShootID)
{
	if (ShootID == SHOOT_NONE)		// 현재 발사되는 것이 없을 때
		return;

	if (!m_Objects[HERO_ID]->CanFireBullet())
	{
		return;
	}

	// 총알의 초기 위치는 HERO의 위치
	float pX, pY, pZ;
	float sX, sY, sZ;
	float vX, vY, vZ;

	m_Objects[HERO_ID]->GetPosition(&pX, &pY, &pZ);
	m_Objects[HERO_ID]->GetSize(&sX, &sY, &sZ);
	
	sX = 0.2f;	//
	sY = 0.2f;
	sZ = 0.2f;

	// 캐릭터가 속도를 가지고 있다면, 속도까지 더해줘서 총알을 움직여줘야 한다.
	m_Objects[HERO_ID]->GetVelocity(&vX, &vY, &vZ);
	float amount = 10.f;

	if (vX < 0)
		vX = 0;

	if (vY < 0)
		vY = 0;

	switch (ShootID)
	{
	case SHOOT_LEFT:
		vX += -amount;
		vY += 0.f;
		break;
	case SHOOT_RIGHT:
		vX += amount;
		vY += 0.f;
		break;
	case SHOOT_UP:
		vX += 0.f;
		vY += amount;
		break;
	case SHOOT_DOWN:
		vX += 0.f;
		vY += -amount;
		break;
	}

	// 총알 생성
	AddObject(
		pX, pY, pZ,
		sX, sY, sZ,
		vX, vY, vZ,
		KIND_BULLET, 
		STATE_GROUND,
		ANIM_STATE_WALK_DOWN,
		BULLET_HP,
		BULLET_HP);
	

	m_Objects[HERO_ID]->InitBulletCooltime();

	m_Sound->PlaySound(m_SoundFire, false, 0.2f);

}

bool ScnMgr::isBoxCollided(float A_minX, float A_minY, float A_minZ, float A_maxX, float A_maxY, float A_maxZ, float B_minX, float B_minY, float B_minZ, float B_maxX, float B_maxY, float B_maxZ)
{
	if (A_maxX < B_minX)
		return false;
	if (B_maxX < A_minX)
		return false;
	if (A_maxY < B_minY)
		return false;
	if (B_maxY < A_minY)
		return false;
	if (A_maxZ < B_minZ)
		return false;
	if (B_maxZ < A_minZ)
		return false;

	return true;
}

void ScnMgr::UpdateCollision()
{

	float A_minX, A_minY, A_maxX, A_maxY, A_minZ, A_maxZ;
	float B_minX, B_minY, B_maxX, B_maxY, B_minZ, B_maxZ;
	float pX, pY, pZ;
	float sX, sY, sZ;
	int A_Kind, B_Kind;

	for (int i = 0; i < MAX_OBJECTS; ++i) {
		int collisionCount = 0;
		if (m_Objects[i] == NULL)
			continue;		
		for (int j = i + 1; j < MAX_OBJECTS; ++j) {		// i와 j가 뒤바뀌어서 한번 더 체크하는 경우 막음 (j= i+1)
			if (m_Objects[j] == NULL)
				continue;
			if (m_Objects[i] != NULL && m_Objects[j] != NULL) {
				m_Objects[i]->GetPosition(&pX, &pY, &pZ);
				m_Objects[i]->GetSize(&sX, &sY, &sZ);
				m_Objects[i]->GetKind(&A_Kind);

				A_minX = pX - sX / 2.f;	A_minY = pY - sY / 2.f;	A_minZ = pZ - sZ / 2.f;
				A_maxX = pX + sX / 2.f;	A_maxY = pY + sY / 2.f;	A_maxZ = pZ + sZ / 2.f;

				m_Objects[j]->GetPosition(&pX, &pY, &pZ);
				m_Objects[j]->GetSize(&sX, &sY, &sZ);
				m_Objects[j]->GetKind(&B_Kind);

				B_minX = pX - sX / 2.f;	B_minY = pY - sY / 2.f;	B_minZ = pZ - sZ / 2.f;
				B_maxX = pX + sX / 2.f;	B_maxY = pY + sY / 2.f;	B_maxZ = pZ + sZ / 2.f;

				if (!(A_Kind == KIND_BUILDING && B_Kind == KIND_BUILDING) && !(A_Kind == KIND_MAP || B_Kind == KIND_MAP)) {
					if (isBoxCollided(A_minX, A_minY, A_minZ, A_maxX, A_maxY, A_maxZ, B_minX, B_minY, B_minZ, B_maxX, B_maxY, B_maxZ)) {
						collisionCount++;
						// Process Collision()가 진행되어야 한다.
						ProcessCollision(i, j);
						/*if (collisionCount > 0 && m_Objects[i] != NULL && m_Objects[j] != NULL && !(A_Kind == KIND_HERO && B_Kind == KIND_BULLET) && !(A_Kind == KIND_BULLET && B_Kind == KIND_BULLET))
						{
							m_Objects[i]->SetColor(1.f, 0.2f, 0.2f, 1.f);
							m_Objects[j]->SetColor(1.f, 0.2f, 0.2f, 1.f);
						}
						else if (collisionCount == 0 && m_Objects[i] != NULL && m_Objects[j] != NULL)
						{
							m_Objects[i]->SetColor(1.f, 1.f, 1.f, 1.f);
							m_Objects[j]->SetColor(1.f, 1.f, 1.f, 1.f);
						}*/
						/*if (collisionCount > 0 && m_Objects[i] != NULL && m_Objects[j] != NULL)
						{
							m_Objects[i]->SetColor(1.f, 0.2f, 0.2f, 1.f);
							m_Objects[j]->SetColor(1.f, 0.2f, 0.2f, 1.f);
						}
						if (collisionCount == 0 && m_Objects[i] != NULL && m_Objects[j] != NULL)
						{
							m_Objects[i]->SetColor(1.f, 1.f, 1.f, 1.f);
							m_Objects[j]->SetColor(1.f, 1.f, 1.f, 1.f);
						}*/
						

				}

				//m_Objects[i]->SetColor(1.f, 1.f, 1.f, 1.f);
				//m_Objects[j]->SetColor(1.f, 1.f, 1.f, 1.f);
				

					
					/*if (m_Objects[i] != NULL && m_Objects[j] != NULL)
					{
						m_Objects[i]->SetColor(1.f, 1.f, 1.f, 1.f);
						m_Objects[j]->SetColor(1.f, 1.f, 1.f, 1.f);
					}*/
				}
				
			}
		}
		if (collisionCount > 0 )
		{
			m_Objects[i]->SetColor(1.f, 0.f, 0.f, 1.f);
		}
		else
		{
			m_Objects[i]->SetColor(1.f, 1.f, 1.f, 1.f);
		}
	}

}

void ScnMgr::ProcessCollision(int i, int j)
{
	Object *obj1 = m_Objects[i];
	Object *obj2 = m_Objects[j];

	int Kind1 = 0, Kind2 = 0;

	int HP1, HP2;
	float m1, m2;
	float pX, pY, pZ;
	float vx1, vy1, vz1, vx2, vy2, vz2;
	float rVx1, rVy1, rVz1, rVx2, rVy2, rVz2;

	if (obj1 == NULL || obj2 == NULL)
	{
		std::cout << "obj " << i << ", obj " << j << ", one of these is null\n";
	}
	obj1->GetKind(&Kind1);
	obj2->GetKind(&Kind2);
	

	// 빌딩 VS 총알
	// 총알 VS 빌딩
	{
		if (Kind1 == KIND_BUILDING && Kind2 == KIND_BULLET)
		{
			
			obj1->GetCurHP(&HP1);
			obj2->GetCurHP(&HP2);

			int newHP = HP1 - HP2;

			obj1->SetCurHP(newHP);
			obj2->SetCurHP(0);


			m_Sound->PlaySound(m_SoundExploision, false, 0.2f);


			std::cout << "총알vs벽" << std::endl;
		}

		if (Kind1 == KIND_BULLET && Kind2 == KIND_BUILDING)
		{
			obj2->GetCurHP(&HP2);
			obj1->GetCurHP(&HP1);

			int newHP = HP2 - HP1;

			obj2->SetCurHP(newHP);
			obj1->SetCurHP(0);



			m_Sound->PlaySound(m_SoundExploision, false, 0.2f);


			std::cout << "총알vs벽" << std::endl;
		}
	}

	// 영웅 vs 부술수 있는 벽
	if (Kind1 == KIND_HERO && Kind2 == KIND_BUILDING)
	{
		obj1->GetMass(&m1);
		obj2->GetMass(&m2);
		obj1->GetVelocity(&vx1, &vy1, &vz1);
		obj2->GetVelocity(&vx2, &vy2, &vz2);

		rVx1 = ((m1 - m2) / (m1 + m2)) * vx1 + ((2.f*m2) / (m1 + m2)) *vx2;
		rVy1 = ((m1 - m2) / (m1 + m2)) * vy1 + ((2.f*m2) / (m1 + m2)) *vy2;
		rVz1 = ((m1 - m2) / (m1 + m2)) * vz1 + ((2.f*m2) / (m1 + m2)) *vz2;

		rVx2 = ((2.f * m2) / (m1 + m2)) * vx1 + ((m2 - m1) / (m1 + m2)) * vx2;
		rVy2 = ((2.f * m2) / (m1 + m2)) * vy1 + ((m2 - m1) / (m1 + m2)) * vy2;
		rVz2 = ((2.f * m2) / (m1 + m2)) * vz1 + ((m2 - m1) / (m1 + m2)) * vz2;

		obj1->SetVelocity(rVx1, rVy1, rVz1);
		obj2->SetVelocity(rVx2, rVy2, rVz2);
	}

	// 영웅 vs 적
	if (Kind1 == KIND_HERO && Kind2 == KIND_ENEMY)
	{
		obj1->GetMass(&m1);
		obj2->GetMass(&m2);
		obj1->GetVelocity(&vx1, &vy1, &vz1);
		obj2->GetVelocity(&vx2, &vy2, &vz2);

		rVx1 = ((m1 - m2) / (m1 + m2)) * vx1 + ((2.f*m2) / (m1 + m2)) *vx2;
		rVy1 = ((m1 - m2) / (m1 + m2)) * vy1 + ((2.f*m2) / (m1 + m2)) *vy2;
		rVz1 = ((m1 - m2) / (m1 + m2)) * vz1 + ((2.f*m2) / (m1 + m2)) *vz2;

		rVx2 = ((2.f * m2) / (m1 + m2)) * vx1 + ((m2 - m1) / (m1 + m2)) * vx2;
		rVy2 = ((2.f * m2) / (m1 + m2)) * vy1 + ((m2 - m1) / (m1 + m2)) * vy2;
		rVz2 = ((2.f * m2) / (m1 + m2)) * vz1 + ((m2 - m1) / (m1 + m2)) * vz2;

		obj1->SetVelocity(rVx1, rVy1, rVz1);
		obj2->SetVelocity(rVx2, rVy2, rVz2);

	}

	// 영웅 vs 왼쪽 벽
	if (Kind1 == KIND_HERO && Kind2 == KIND_BACKGROUND_LR)
	{
		obj1->GetVelocity(&vx1, &vy1, &vz1);
		obj1->SetVelocity(-vx1, vy1, vz1);
	}

	// 영웅 vs 오른쪽 벽

	if (Kind1 == KIND_HERO && Kind2 == KIND_BACKGROUND_UD)
	{
		obj1->GetVelocity(&vx1, &vy1, &vz1);
		obj1->SetVelocity(vx1, -vy1, vz1);
	}

	// 총알 vs 왼오 벽
	// 총알 vs 위아 벽
	{
		if (Kind1 == KIND_BULLET && Kind2 == KIND_BACKGROUND_LR)
		{
			delete(obj1);
		}

		if (Kind1 == KIND_BULLET && Kind2 == KIND_BACKGROUND_UD)
		{
			delete(obj1);
		}

		if (Kind1 == KIND_BACKGROUND_LR && Kind2 == KIND_BULLET)
		{
			delete(obj2);
		}
		if (Kind1 == KIND_BACKGROUND_UD && Kind2 == KIND_BULLET)
		{
			delete(obj2);
		}
	}

	// 영웅 vs 문
	{
		if (Kind1 == KIND_HERO && Kind2 == KIND_DOOR_12)
		{
			obj2->GetPosition(&pX, &pY, &pZ);
			pX += 3.5f;
			obj1->SetPosition(pX, pY, pZ);
		}

		if (Kind1 == KIND_HERO && Kind2 == KIND_DOOR_21)
		{
			obj2->GetPosition(&pX, &pY, &pZ);
			pX -= 3.5f;
			obj1->SetPosition(pX, pY, pZ);
		}

		if (Kind1 == KIND_HERO && Kind2 == KIND_DOOR_34)
		{
			obj2->GetPosition(&pX, &pY, &pZ);
			pX += 3.5f;
			obj1->SetPosition(pX, pY, pZ);
		}

		if (Kind1 == KIND_HERO && Kind2 == KIND_DOOR_43)
		{
			obj2->GetPosition(&pX, &pY, &pZ);
			pX -= 3.5f;
			obj1->SetPosition(pX, pY, pZ);
		}

	}


	if (Kind1 == KIND_HERO && (KIND_FIRE_D <= Kind2 && Kind2 <= KIND_FIRE_R))
	{
		obj1->GetCurHP(&HP1);
		obj2->GetCurHP(&HP2);
		int newHP = HP1 - HP2;

		obj1->SetCurHP(newHP);
		obj2->SetCurHP(0);

		m_Sound->PlaySound(m_SoundExploision, false, 0.2f);

		std::cout << "불vs캐릭터" << std::endl;
	}
	

}

void ScnMgr::DoGarbageCollect()
{
	

	for (int i = 0; i < MAX_OBJECTS; ++i) {

		if (m_Objects[i] == NULL)
			continue;	// 그냥 포문을 돈다.

		float pX, pY, pZ, vX, vY, vZ, vel;	// vel은 속도의 크기
		float sX, sY, sZ;
		int kind, HP;

		m_Objects[i]->GetPosition(&pX, &pY, &pZ);
		m_Objects[i]->GetSize(&sX, &sY, &sZ);
		m_Objects[i]->GetVelocity(&vX, &vY, &vZ);
		m_Objects[i]->GetKind(&kind);
		m_Objects[i]->GetCurHP(&HP);

		vel = sqrtf(vX * vX + vY * vY + vZ * vZ);

		// Check Velocity
		if (vel < FLT_EPSILON && kind == KIND_BULLET)	// 0일 경우
		{
			DeleteObject(i);
			continue;	// 삭제된 것 또 체크 할 필요는 없음. 포문 시작점으로 옮겨가는 듯
		}

		// Check HP
		if (HP <= 0 && (kind == KIND_BULLET || kind == KIND_BUILDING || (KIND_FIRE_D <= kind && kind <= KIND_FIRE_R))) 
		{
			DeleteObject(i);
			continue;
		}
		float m_HeightMeter = 50.f;
		float m_WidthMeter = 50.f;

		// check outofBound
		if (pX < -m_WidthMeter / 2.f || pX > m_WidthMeter / 2.f || pY < - m_HeightMeter / 2.f || pY > m_HeightMeter / 2.f)	// 5미터, 2.5미터 (화면은 10미터, 5미터)
		{
			if (kind == KIND_BULLET || (KIND_FIRE_D <= kind && kind <= KIND_FIRE_R))
			{
				DeleteObject(i);
				std::cout << "총알 밖으로 나가 삭제됨" << std::endl;

				continue;
			}
		}

	}
}

void ScnMgr::SelectButton(bool Spacebar)
{
	IsSelectButtonPressed = Spacebar;

	if (g_GameState == GAME_STATE_TITLE && IsSelectButtonPressed)
		g_GameState = GAME_STATE_INGAME;

	if (g_GameState == GAME_STATE_END && IsSelectButtonPressed) {
		
		g_GameState = GAME_STATE_INGAME;
		m_Objects[HERO_ID]->SetPosition(-14.5f, 3.f, 0.f);
		m_Objects[HERO_ID]->SetVelocity(0.f, 0.f, 0.f);
		m_Objects[HERO_ID]->SetAcc(0.f, 0.f, 0.f);
		m_Objects[HERO_ID]->SetSize(0.5f, 0.5f, 0.5f);				//1pixel 당 1cm로 기준을 잡을 것 ...
		m_Objects[HERO_ID]->SetForce(0.f, 0.f, 0.f);
		m_Objects[HERO_ID]->SetMass(0.01f);
		m_Objects[HERO_ID]->SetFrictionCoef(0.8f);
		m_Objects[HERO_ID]->SetKind(KIND_HERO);
		m_Objects[HERO_ID]->SetState(STATE_GROUND);
		m_Objects[HERO_ID]->SetAnimState(ANIM_STATE_IDLE_DOWN);
		m_Objects[HERO_ID]->SetColor(1.f, 1.f, 1.f, 1.f);
		m_Objects[HERO_ID]->SetCurHP(HERO_HP);
	}
}

