#pragma once

#include "Renderer.h"
#include "Object.h"

#include "Global.h"
#include "Sound.h"


class ScnMgr {
	Renderer *m_Renderer;

	int m_ScreenWidth;
	int m_ScreenHeight;

	bool IsSelectButtonPressed = false;

	Sound *m_Sound;
	Object *m_Objects[MAX_OBJECTS];

	// GLuint m_TestTexture;
	GLuint m_Title = 0;
	GLuint m_End = 0;
	GLuint m_Map = 0;
	GLuint m_Texture = 0;
	GLuint m_SeqTexture = 0;
	GLuint m_BulletTexture = 0;
	GLuint m_WallTexture = 0;

	// Mario Textures
	GLuint m_Mario_Idle = 0;
	GLuint m_Mario_Run = 0;

	// Link Anims
	GLuint m_Link_Walk_D = 0;
	GLuint m_Link_Walk_U = 0;
	GLuint m_Link_Walk_L = 0;
	GLuint m_Link_Walk_R = 0;

	GLuint m_Link_Idle_D = 0;
	GLuint m_Link_Idle_U = 0;
	GLuint m_Link_Idle_L = 0;
	GLuint m_Link_Idle_R = 0;

	GLuint m_Link_Shot_D = 0;
	GLuint m_Link_Shot_U = 0;
	GLuint m_Link_Shot_L = 0;
	GLuint m_Link_Shot_R = 0;

	// HP
	GLuint m_Heart_Full = 0;
	GLuint m_Heart_Half = 0;
	GLuint m_Heart_Empty = 0;

	// WALL
	GLuint m_block = 0;
	GLuint m_block_UD = 0;
	GLuint m_door = 0;

	// Enemy
	GLuint m_Fire = 0;

	




	int m_SoundBG = 0;
	int m_SoundFire = 0;
	int m_SoundExploision = 0;

public:
	ScnMgr(int width, int height);
	~ScnMgr();
	void RenderScene();
	void Update(float elapsedTime);
	void ApplyForce(float x, float y, float z, float elapsedTime);
	void AddObject(float pX, float pY, float pZ, float sX, float sY, float sZ, float vX, float vY, float vZ, int kind, int state, int animState, int maxHP, int hp);	// 기본적으로 필요한 위치, 크기값, 속도값 초기화 할 수 있게
	void LatestDirectionInput(int dir);
	void DeleteObject(unsigned int id);		// id를 이용해서 지울것임
	int FindEmptyObjectSlot();		// 배열중에 비어있는

	void Shoot(int ShootID);

	bool isBoxCollided(float A_minX, float A_minY, float A_minZ, float A_maxX, float A_maxY, float A_maxZ, float B_minX, float B_minY, float B_minZ, float B_maxX, float B_maxY, float B_maxZ);
	void UpdateCollision();
	void ProcessCollision(int i, int j);
	void DoGarbageCollect();

	void SelectButton(bool Spacebar);

};

