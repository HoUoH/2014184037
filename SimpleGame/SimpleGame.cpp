/*
Cog_Yright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "time.h"

#include "ScnMgr.h"
#include "Sound.h"

ScnMgr *g_ScnMgr = NULL;
DWORD g_PrevRenderTime = 0;
float stackShoot_eTime = 0;

bool g_KeyW = false;	// 눌림 = true, 아님 = false
bool g_KeyS = false;
bool g_KeyA = false;
bool g_KeyD = false;
bool g_KeySpaceBar = false;

int g_LatestAnim = ANIM_STATE_IDLE_DOWN;

////////////////////////
int g_ShootKey = SHOOT_NONE;
////////////////////////

// timeGetTime : ms단위로 컴퓨터를 키고나서부터 현재까지 지난 시간을 재주는 함수


void RenderScene(void)								// 렌더링의 순서 : 지우고 - 렌더링 - 띄우고(반복)
{

	if (g_PrevRenderTime == 0)						// Initialize
		g_PrevRenderTime = timeGetTime();

	// Elapsed Time
	DWORD currentTime = timeGetTime();
	DWORD elapsedTime = currentTime - g_PrevRenderTime;
	//g_PrevRenderTime = currentTime;
	float eTime = (float)elapsedTime / 1000.f;	   	// convert to second

	if (eTime < 0.016f) {
		return;
	}

	g_PrevRenderTime = currentTime;

	//std::cout << "elapsedTime : " << eTime << "\n";

	float forceX = 0.f, forceY = 0.f, forceZ = 0.f;
	float amount = 0.2f;

	if (g_KeyW)
	{
		forceY += amount;
	}
	if (g_KeyS)
	{
		forceY -= amount;
	}
	if (g_KeyA)
	{
		forceX -= amount;
	}
	if (g_KeyD)
	{
		forceX += amount;
	}
	
	if (g_KeyW == false && g_LatestAnim == ANIM_STATE_WALK_UP)
	{
		g_LatestAnim = ANIM_STATE_IDLE_UP;
	}

	if (g_KeyS == false && g_LatestAnim == ANIM_STATE_WALK_DOWN)
	{
		g_LatestAnim = ANIM_STATE_IDLE_DOWN;
	}

	if (g_KeyA == false && g_LatestAnim == ANIM_STATE_WALK_LEFT)
	{
		g_LatestAnim = ANIM_STATE_IDLE_LEFT;
	}

	if (g_KeyD == false && g_LatestAnim == ANIM_STATE_WALK_RIGHT)
	{
		g_LatestAnim = ANIM_STATE_IDLE_RIGHT;
	}

	///////
	if (g_ShootKey == SHOOT_DOWN )
	{
		if (g_KeyW)
			g_LatestAnim = ANIM_STATE_SHOT_DOWN;
		if (g_KeyS)
			g_LatestAnim = ANIM_STATE_SHOT_DOWN;
		if (g_KeyA)
			g_LatestAnim = ANIM_STATE_SHOT_DOWN;
		if (g_KeyD)
			g_LatestAnim = ANIM_STATE_SHOT_DOWN;
	}

	if (g_ShootKey == SHOOT_UP)
	{
		if (g_KeyW)
			g_LatestAnim = ANIM_STATE_SHOT_UP;
		if (g_KeyS)						   
			g_LatestAnim = ANIM_STATE_SHOT_UP;
		if (g_KeyA)						   
			g_LatestAnim = ANIM_STATE_SHOT_UP;
		if (g_KeyD)						   
			g_LatestAnim = ANIM_STATE_SHOT_UP;
	}

	if (g_ShootKey == SHOOT_LEFT)
	{
		if (g_KeyW)
			g_LatestAnim = ANIM_STATE_SHOT_LEFT;
		if (g_KeyS)						   
			g_LatestAnim = ANIM_STATE_SHOT_LEFT;
		if (g_KeyA)						   
			g_LatestAnim = ANIM_STATE_SHOT_LEFT;
		if (g_KeyD)						   
			g_LatestAnim = ANIM_STATE_SHOT_LEFT;
	}

	if (g_ShootKey == SHOOT_RIGHT)
	{
		if (g_KeyW)
			g_LatestAnim = ANIM_STATE_SHOT_RIGHT;
		if (g_KeyS)						   
			g_LatestAnim = ANIM_STATE_SHOT_RIGHT;
		if (g_KeyA)						   
			g_LatestAnim = ANIM_STATE_SHOT_RIGHT;
		if (g_KeyD)						   
			g_LatestAnim = ANIM_STATE_SHOT_RIGHT;
	}

	if (g_ShootKey == SHOOT_NONE && g_LatestAnim == ANIM_STATE_SHOT_UP)
	{
		g_LatestAnim = ANIM_STATE_IDLE_UP;
	}

	if (g_ShootKey == SHOOT_NONE && g_LatestAnim == ANIM_STATE_SHOT_DOWN)
	{
		g_LatestAnim = ANIM_STATE_IDLE_DOWN;
	}

	if (g_ShootKey == SHOOT_NONE && g_LatestAnim == ANIM_STATE_SHOT_LEFT)
	{
		g_LatestAnim = ANIM_STATE_IDLE_LEFT;
	}

	if (g_ShootKey == SHOOT_NONE && g_LatestAnim == ANIM_STATE_SHOT_RIGHT)
	{
		g_LatestAnim = ANIM_STATE_IDLE_RIGHT;
	}

	
	g_ScnMgr->LatestDirectionInput(g_LatestAnim);

	g_ScnMgr->ApplyForce(forceX, forceY, forceZ, eTime);

	g_ScnMgr->Update(eTime);
	
	g_ScnMgr->RenderScene();

	g_ScnMgr->DoGarbageCollect();

	g_ScnMgr->Shoot(g_ShootKey);

	glutSwapBuffers();

	// 키 입력에 대한 정보 출력
	/*std::cout << "w:" << g_KeyW << " "
		<< "s:" << g_KeyS << " "
		<< "a:" << g_KeyA << " "
		<< "d:" << g_KeyD << " "
		<< "SP:" << g_KeySpaceBar << "\n";

	std::cout << "up:" << g_ShootKey << "\n";*/

	
}

void Idle(void)
{
	RenderScene();
}

void MouseInput(int button, int state, int x, int y)
{
	RenderScene();
}

void KeyDownInput(unsigned char key, int x, int y)	// 인자는 하나인데, 동시 키입력을 받을 수 있는 이유가, 사람이 아무리 빨라고 ns단위로 동시에 못눌러서.
{
	//float amount = 0.5f;

	if (key == 'w' || key == 'W')
	{
		g_KeyW = true;
		//g_ScnMgr->ApplyForce(0.f, +amount);
		g_LatestAnim = ANIM_STATE_WALK_UP;

	}
	if (key == 's' || key == 'S')
	{
		g_KeyS = true;
		//g_ScnMgr->ApplyForce(0.f, -amount);
		g_LatestAnim = ANIM_STATE_WALK_DOWN;

	}
	if (key == 'a' || key == 'A')
	{
		g_KeyA = true;
		//g_ScnMgr->ApplyForce(-amount, 0.f);
		g_LatestAnim = ANIM_STATE_WALK_LEFT;

	}
	if (key == 'd' || key == 'D')
	{
		g_KeyD = true;
		//g_ScnMgr->ApplyForce(+amount, 0.f);
		g_LatestAnim = ANIM_STATE_WALK_RIGHT;
	}
	if (key == ' ')
	{
		g_KeySpaceBar = true;
		g_ScnMgr->SelectButton(g_KeySpaceBar);
	}
	// 키인풋에서 RenderScene안부르고, 이제 키 입력만 처리하는 함수로 바꿀것임.
	//RenderScene();

	g_ScnMgr->LatestDirectionInput(g_LatestAnim);

}

void KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		g_KeyW = false;
	}
	if (key == 's' || key == 'S')
	{
		g_KeyS = false;
	}
	if (key == 'a' || key == 'A')
	{
		g_KeyA = false;
	}
	if (key == 'd' || key == 'D')
	{
		g_KeyD = false;
	}
	if (key == ' ')
	{
		g_KeySpaceBar = false;
		g_ScnMgr->SelectButton(g_KeySpaceBar);

	}

}
////////////////////////

void SpecialKeyDownInput(int key, int x, int y)
{
	if (key == GLUT_KEY_LEFT)
	{
		g_ShootKey = SHOOT_LEFT;
		g_LatestAnim = ANIM_STATE_SHOT_LEFT;
	}
	else if (key == GLUT_KEY_RIGHT)
	{
		g_ShootKey = SHOOT_RIGHT;
		g_LatestAnim = ANIM_STATE_SHOT_RIGHT;
	}
	else if (key == GLUT_KEY_UP)
	{
		g_ShootKey = SHOOT_UP;
		g_LatestAnim = ANIM_STATE_SHOT_UP;
	}
	else if (key == GLUT_KEY_DOWN)
	{
		g_ShootKey = SHOOT_DOWN;
		g_LatestAnim = ANIM_STATE_SHOT_DOWN;
	}

}


void SpecialKeyUpInput(int key, int x, int y)
{
	// 키보드가 떼지면 NONE으로 바꿔줌
	g_ShootKey = SHOOT_NONE;
}
////////////////////////

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(WINDOW_SIZEX, WINDOW_SIZEY);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0")) {
		std::cout << " GLEW Version is 3.0\n ";
	}
	else {
		std::cout << "GLEW 3.0 not supported\n ";
	}

	g_ScnMgr = new ScnMgr(WINDOW_SIZEX, WINDOW_SIZEY);

	// 키보드
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	// 이름 명확히, 함수 세분화(다운, 업)
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);

	glutMouseFunc(MouseInput);
	////////////////////////
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);
	////////////////////////

	glutMainLoop();

	delete g_ScnMgr;

    return 0;
}

