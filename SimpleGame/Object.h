#pragma once

#include <iostream>


class Object {
	float m_PosX;	float m_PosY;	float m_PosZ;

	float m_SizeX;	float m_SizeY;	float m_SizeZ;

	float m_R = 1.f;	float m_G = 1.f;	float m_B = 1.f;	float m_A =1.f;

	float m_VelX, m_VelY, m_VelZ;
	float m_AccX, m_AccY, m_AccZ;
	
	float m_ForceX, m_ForceY, m_ForceZ;
	float m_Mass;

	float m_FrictioncCoef;

	int m_Kind;

	int m_MaxHP;
	int m_CurHP;

	int m_State;
	int m_AnimState;

	float m_BulletCoolTime;
	float m_BulletTime;

	


public:
	Object();
	~Object();

	void Update(float eTimeInSec);
	void ApplyForce(float x, float y, float z, float elapsedTime);

	void GetPosition(float *x, float *y, float *z);
	void SetPosition(float x, float y, float z);

	void GetSize(float *x, float *y, float *z);
	void SetSize(float x, float y, float z);

	void GetVelocity(float *vX, float *vY, float *vZ);
	void SetVelocity(float vX, float vY, float vZ);

	void GetAcc(float *aX, float *aY, float *aZ);
	void SetAcc(float aX, float aY, float aZ);
	   
	void GetForce(float *fX, float *fY, float *fZ);
	void SetForce(float fX, float fY, float fZ);

	void GetMass(float* mass);
	void SetMass(float mass);

	void GetFrictionCoef(float *friction);
	void SetFrictionCoef(float friction);

	void GetKind(int *kind);
	void SetKind(int kind);

	void GetColor(float *R, float *G, float *B, float *A);
	void SetColor(float R, float G, float B, float A);

	void GetMaxHP(int *MaxHP);
	void SetMaxHP(int MaxHP);

	void GetCurHP(int *CurHP);
	void SetCurHP(int CurHP);

	void GetState(int *state);
	void SetState(int state);

	void GetAnimState(int *AnimState);
	void SetAnimState(int AnimState);

	void InitBulletCooltime();
	bool CanFireBullet();
};